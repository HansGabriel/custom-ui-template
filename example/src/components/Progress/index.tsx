import React, { FC } from "react";
import { Progress as ChakraProgress, useColorModeValue } from "@chakra-ui/react";
import { ProgressColorScheme, ProgressSizes } from "../../lib/types";

interface LoadersPropTypes {
  color: ProgressColorScheme;
  size: ProgressSizes;
  value: number;
  isIndeterminate: boolean;
  min: number;
  max: number;
  isRound: boolean;
}

export const Progress:FC<LoadersPropTypes> = ({
  color,
  size,
  value,
  isIndeterminate,
  min,
  max,
  isRound
}: LoadersPropTypes) => {
  return (
    <ChakraProgress
      min={min}
      max={max}
      isIndeterminate={isIndeterminate}
      colorScheme={color}
      size={size}
      value={value}
      sx={{backgroundColor: useColorModeValue("#EBEFFF","#324073") }}
      borderRadius={isRound? 50 : 0}
    />
  );
};
