import { ChipsVariant, ChipsSizes } from '../../lib/types'
import {
  Avatar,
  Tag,
  TagCloseButton,
  TagLabel,
  useColorMode,
} from '@chakra-ui/react'
import { FC } from 'react'

interface LoadersPropTypes {
  variant?: ChipsVariant
  size?: ChipsSizes
  avatar?: string
  label: string
  isClosable?: boolean
  onClose?: () => void
}

export const Chips: FC<LoadersPropTypes> = ({
  variant,
  size,
  avatar,
  label,
  isClosable,
  onClose,
}) => {
  const { colorMode } = useColorMode()
  return (
    <Tag variant={variant} size={size} borderRadius="full">
      {!!avatar && (
        <Avatar src={avatar} size="xs" name={label} ml={-1} mr={2} />
      )}
      <TagLabel>{label}</TagLabel>
      {isClosable && (
        <TagCloseButton
          color="inherit"
          bg={colorMode === 'dark' ? 'transparent' : 'frethanWhite'}
          onClick={onClose}
        />
      )}
    </Tag>
  )
}
