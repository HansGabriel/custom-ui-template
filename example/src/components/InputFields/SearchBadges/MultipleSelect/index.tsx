import { BadgeSelectProps, BadgesVariants, BadgeVariant, OnRemoveItemParams, SelectedItemsState } from '@/lib/types'
import { getBadgeVariantProps } from '@/lib/utils'
import {
  HStack,
  Box,
  Text,
  Input,
  BoxProps,
  Icon
} from '@chakra-ui/react'
import { ChangeEvent, KeyboardEvent, MouseEvent, useEffect, useMemo, useRef, useState } from 'react'
import { PlusIncircleFilledIcon, SearchIcon, TimesIcon } from '@/components/icons'

interface MultipleSelectProps {
  variant?: BadgesVariants
  inputProps?: Omit<BoxProps, 'height' | 'overflowY' | 'overflowX'>
}

export const MultipleSelect = ({
  variant,
  inputProps
}: MultipleSelectProps) => {
  const inputRef = useRef<HTMLInputElement>(null)
  const containerRef = useRef<HTMLDivElement>(null)
  const currentVariant = variant || BadgeVariant.PLAIN
  const [currentCount, setCurrentCount]: [number, (value: number) => void] = useState<number>(0)
  const [textValue, setTextValue]: [string, (value: string) => void] = useState<string>('')
  const [isShowList, setIshShowList]: [boolean, (isShow: boolean) => void] = useState<boolean>(true)
  const [selectedItems, setSelectedItems]: SelectedItemsState = useState<BadgeSelectProps[] | []>([])

  useEffect(() => {
    const handleClickOutside = (event: any) => {
      if (containerRef.current && !containerRef.current.contains(event.target)) {
        setTextValue('')
        setIshShowList(true)
      }
    }
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [containerRef])

  const badgeVariantProps = useMemo(() => {
    return getBadgeVariantProps(currentVariant)
  }, [currentVariant])

  const onClickContainer = async () => {
    const showListPromise = new Promise((resolve) => {
      setIshShowList(false)
      resolve('')
    })

    showListPromise.then(() => {
      inputRef.current?.focus()
    })
  }

  const onClickItem = (eventProps?: MouseEvent) => {
    if (eventProps) {
      eventProps.stopPropagation()
    }
    
    const newCount = currentCount + 1
    setCurrentCount(newCount)

    const selectedItem: BadgeSelectProps = {
      id: newCount,
      value: textValue
    }
    setTextValue('')
    setSelectedItems([...selectedItems, selectedItem])
    setIshShowList(true)
  }

  const onRemoveItem = ({
    id,
    eventProps
  }: OnRemoveItemParams) => {
    eventProps.stopPropagation()
    const newSelectedItems = selectedItems.filter(details => details.id !== id)
    setSelectedItems(newSelectedItems)
    setIshShowList(true)
  }

  const onTextChange = (event: ChangeEvent<HTMLInputElement>) => {
    setTextValue(`${event.target.value}`)
  }

  const onKeyDown = (event: KeyboardEvent<HTMLInputElement>) => {
    if (event.key === 'Enter') {
      onClickItem()
    }
  }

  return (
    <>
      <HStack
        ref={containerRef}
        zIndex="10"
        position="relative"
        border={badgeVariantProps.BORDER}
        borderColor={badgeVariantProps.BORDER_COLOR}
        bgColor={isShowList ? badgeVariantProps.BACKGROUND_COLOR : 'frethanWhite'}
        borderRadius={badgeVariantProps.BORDER_RADIUS}
        padding={badgeVariantProps.PADDING}
        paddingLeft={8}
        boxShadow={badgeVariantProps.BOX_SHADOW}
        h="40px"
        onClick={onClickContainer}
        {...inputProps}
        overflowY="hidden"
        overflowX="hidden"
        _hover={{
          cursor: 'pointer',
          overflowX: 'auto'
        }}
        css={{
          '&::-webkit-scrollbar-track': {
            '-webkit-box-shadow:': 'inset 0 0 6px rgba(0,0,0,0.3)',
            borderRadius: '10px',
            backgroundColor: 'frethanBlack'
          },
          '&::-webkit-scrollbar': {
            width: '12px',
            height: '8px',
            position: 'relative',
            zIndex: '1000',
            backgroundColor: 'frethanSpringWood',
            transition: '0.03s'
          },
          '&::-webkit-scrollbar-thumb': {
            borderRadius: '10px',
            '-webkit-box-shadow': 'inset 0 0 6px rgba(0,0,0,.3)',
            backgroundColor: 'frethanPersianRed'
          }
        }}
      >
        <SearchIcon style={{position: 'absolute', left: 12 }}/>
        {
          isShowList && selectedItems && !!selectedItems.length && selectedItems.map((details, key) => (
            <HStack
              key={key}
              alignItems="cetner"
              justifyContent="center"
              p="4px 8px"
              spacing={4}
              bgColor={variant === BadgeVariant.DANGER ? 'frethanCrimsonRed' : 'frethanSeashell'}
              borderRadius="8px"
              flexShrink="0"
            >
              <Text
                variant="labelMed"
                color={variant === BadgeVariant.DANGER ? 'frethanWhite' : 'frethanBlack'}
                w="fit-content"
                h="fit-content"
                textAlign="center"
              >
                {details.value}
              </Text>
              <Icon
                as={TimesIcon}
                w="10.67px"
                zIndex="1000"
                color={variant === BadgeVariant.DANGER ? 'frethanWhite' : 'frethanBlack'}
                onClick={(event) => onRemoveItem({
                  eventProps: event,
                  id: details.id
                })}
              />
            </HStack>
          ))
        }
        {
          !isShowList && (
            <HStack w="100%" pr="10px">
              <Input 
                ref={inputRef}
                variant="unstyled"
                value={textValue}
                onChange={onTextChange}
                onKeyDown={(event) => onKeyDown(event)}
              />
              <Icon 
                as={PlusIncircleFilledIcon} 
                w="20px" 
                h="20px"
                onClick={(event) => onClickItem(event)}
              />
            </HStack>
          )
        }
      </HStack>
    </>
  )
}