import React, { FC } from "react";
import { Radio, RadioProps } from "@chakra-ui/react";
import { RadioVariants, RadiosSizes } from "../../lib/types";
interface InputFieldProps {
  label: string;
  size?: RadiosSizes;
  disabled?: boolean;
  isInvalid?: boolean;
  radioProps?: RadioProps
}

export const Radios:FC<InputFieldProps> = ({ 
  label, 
  size, 
  disabled, 
  isInvalid, 
  radioProps 
}: InputFieldProps) => {
  return (
    <Radio 
      size={size} 
      isDisabled={disabled} 
      isInvalid={isInvalid} 
      {...radioProps}
    >
      {label}
    </Radio>
  );
};
