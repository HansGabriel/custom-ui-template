import { MouseEvent } from "react"

export enum BadgeVariant {
  PLAIN = 'plain',
  SOLID = 'solid',
  GLOW = 'glow',
  DANGER = 'danger',
  DISABLED = 'disabled'
}

export type BadgesVariants = 'plain' | 'solid' | 'glow' | 'danger' | 'disabled'

export interface BadgeSelectProps {
  id: number
  value: string
}

export type SetSelectedItems = (selectionList: BadgeSelectProps[] | []) => void
export type SelectedItemsState = [BadgeSelectProps[] | [], SetSelectedItems]

export interface OnRemoveItemParams {
  eventProps: MouseEvent
  id: number
}
