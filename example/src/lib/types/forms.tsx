import { MouseEventHandler } from "react";
import { ComponentWithAs, IconProps, ColorMode } from "@chakra-ui/react";
import { USER_ROLES } from "./signup";

export interface InputIconProps {
  icon?: ComponentWithAs<'svg', IconProps> | any
  color?: string
  width: string
  height: string
  iconProps?: IconProps,
  action?: MouseEventHandler | undefined
}

export type InputFieldVariant =
  | "default"
  | "plain"
  | "solidBlue"
  | "solidBlueSecondary"
  | "glowBlue"
  | "solidGrey"
  | "dangerField"
  | "disabledField"
  | "successField";

  export type SelectFieldVariant =
  | "default"
  | "plain"
  | "solidBlue"
  | "solidBlueSecondary"
  | "glowBlue"
  | "solidGrey"
  | "dangerField"
  | "disabledField"
  | "successField";

export type SteppersVariant =
  | "default"
  | "solidBluePlainText"
  | "solidGreyPlainText"

export interface GetInputFieldIconColorParams {
  isDisabled?: boolean,
  isInvalid?: boolean,
  isSuccess?: boolean
}

export interface ButtonColorProps {
  background: string;
  color: string;
}

export interface InputGroupButtonProps {
  label: string;
  func?: () => void;
  colorProps?: ButtonColorProps;
}

export enum InputGroupSizes {
  LG = "lg",
  MD = "md",
  SM = "sm",
}

export type InputGroupSize = "lg" | "md" | "sm";

export interface InputGroupFieldProps {
  inputHeight: string;
  buttonHeight: string;
}

export interface StepperFieldProps {
  inputHeight: string;
  buttonProps: string;
}

export type SetCountInputState = (currentCount: number) => void;
export type CounterInputState = [number, SetCountInputState];

export enum CounterTypes {
  INCREASE = "increase",
  DECREASE = "decrease",
}

export type CounterType = CounterTypes.INCREASE | CounterTypes.DECREASE;

export interface CountryType {
  code: string;
  label: string;
  phone: string;
  suggested?: boolean;
}

export interface GetStepperFontColorsProps {
  isInvalid?: boolean
  isDisabled?: boolean
  isSolid?: boolean
  colorMode?: ColorMode
}

export interface OptionsProps {
  name: string
  value: string | number
}

export interface MobileNumberProps {
  areaCode: string
  number: string
}

export interface BuyerFormData {
  firstname: string
  lastname: string
  email: string
  password: string
  deliveryAddressProvince: string
  deliveryAddressCity: string
  deliveryAddressCountry: string
  mobileNumber: MobileNumberProps
  profilePhoto: string
  companyName: string
}

export interface SupplierMobileNumberProps {
  regionCode: string
  number: number
}

export interface SupplierCompanyAddressProps {
  province: string
  city: string
  roadDetails: string
}

export interface SupplierFormData {
  countryRegion: string
  password: string
  confirmPassword: string
  mobileNumber: SupplierMobileNumberProps
  companyName: string
  firstname: string
  lastname: string
  email: string
  companyAddress: SupplierCompanyAddressProps
  logoPhoto: string
  idPhoto: string
}

export type UserRole = 'buyer' | 'supplier'

export interface LoginSubmitProps { 
  username: string
  password: string 
  submitType?: UserRole
}

export interface AuthCodeSubmitProps {
  authCode: string
}

export interface PixelCropProps {
  width: number
  height: number
  x: number
  y: number
}

export interface GetCroppedImgParams {
  imageSrc: string
  pixelCrop: PixelCropProps
  rotation: number
}

export type SetCroppedPropsFunc = (cropProps: PixelCropProps | null) => void
export type CroppedPropsState = [PixelCropProps | null, SetCroppedPropsFunc]

export enum SignupStep {
  USER_DETAILS = 1,
  VERIFY_TYPE = 2,
  VERIFY = 3,
  CATEGORIES = 4,
  ACC_REVIEW = 5
}

export enum VerificationStep {
  SMS = 'sms',
  EMAIL = 'email'
}

export interface ConfirmVerificationFormProps {
  verifyAuthCode: string
}

export interface CategoriesProps {
  id: number
  mobileRegionCode: string
  abbrev: string
  name: string
  mobileName: string
}

export interface MenuSelectOptionProps {
  id: number
  name: string
}

export type SupplierFormFieldProps = "countryRegion" | 
  "password" | 
  "confirmPassword" | 
  "mobileNumber" | 
  "companyName" | 
  "firstname" | 
  "lastname" | 
  "email" | 
  "companyAddress" | 
  "mobileNumber.number" | 
  "mobileNumber.regionCode" | 
  "companyAddress.province" | 
  "companyAddress.city" | 
  "companyAddress.country" | 
  "companyAddress.roadDetails"

  export type SubmitType = USER_ROLES.BUYER | USER_ROLES.SUPPLIER | USER_ROLES.ADMIN | USER_ROLES.SUPER_ADMIN | null

  export interface CategoryItemProps {
    id: number
    name: string
    icon: string
  }

  export interface TagProps {
    name: string
    value: number | string
  }

  export interface SendInquirySampleFormProps {
    isSamples?: string
    size: number
    color: string
    sampleText: string
    otherText: string,
    suppliers: string[],
  }

  export interface SendInquiryFormProps {
    suppliers: string[],
    productDescription: string,
    fileAttachment: File[],
    isSamples: string
    samples: SendInquirySampleFormProps
  }

  export interface DataUrlToFileParams {
    dataurl: string
    filename: string
    mimeType: string
  }

  export interface ReplyMessageData {
    message: string
  }