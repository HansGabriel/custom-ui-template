export const regexValidator = {
    name: "^[A-Za-z]{1,}(\\d*|([ a-zA-Z]{2,})*)$",
    email: "^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,})+$",
    phone: "^[+]?[(]?\\d{3}[)]?[-\\s\\.]?\\d{3}[-\\s\\.]?\\d{4,6}$",
    ein: "^(0[1-6]|1[0-6]|2[0-7]|[345]\\d|[68][0-8]|7[1-7]|9[0-58-9])-?\\d{7}$",
    password: "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$"
  }

export const formValidator = (key: string, value: any) => {
    if (key === "firstname" || key === "lastname") {
      return new RegExp(regexValidator.name).test(value);
    } else if (key === "email-strict") {
      return new RegExp(regexValidator.email).test(value);
    } else if (key === "phone") {
        return new RegExp(regexValidator.phone).test(value);
    } else if (key === "password-strict") {
        return new RegExp(regexValidator.password).test(value);
    } else {
        return true
    }
  };

  export const emailValidator = (email: string) => {
    return String(email)
    .toLowerCase()
    .match(
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    )
  }