import { CheckCircleFilledIcon, ThunderIcon } from "../../icons";
import colors from "../../components/themes/";
import { BADGES_VARIANTS, INPUT_GROUP_FIELD_PROPS, INPUT_GROUP_SIZES } from "../../constants/forms";
import { ComponentWithAs, IconProps } from "@chakra-ui/react";
import { BadgesVariants, BadgeVariant, ButtonColorProps, CheckboxSizes, CheckboxVariants, GetInputFieldIconColorParams, GetStepperFontColorsProps, InputFieldVariant, InputGroupFieldProps, InputGroupSize, SignupStep } from "../types";

export const getIconColor = (variant: InputFieldVariant): string => {

  switch (variant) {
    case 'dangerField':
      return 'frethanCrimsonRed'
    case 'disabledField':
      return 'frethanGrey'
    case 'successField':
      return 'frethanPositiveGreen'
    default:
      return 'frethanBlack'
  }
}

export const getInputFieldIconColor = ({
  isDisabled = false,
  isInvalid = false,
  isSuccess = false
}: GetInputFieldIconColorParams) => {
  if (isDisabled) {
    return 'frethanGrey'
  }

  if (isInvalid) {
    return 'frethanCrimsonRed'
  }

  if (isSuccess) {
    return 'frethanPositiveGreen'
  }
}

export const getLeftIcon = (variant: InputFieldVariant): ComponentWithAs<'svg', IconProps> => {
  if (variant === 'successField') {
    return CheckCircleFilledIcon
  }
  return ThunderIcon
}

export const getInputHeight = (size: InputGroupSize): InputGroupFieldProps => {
  switch (size) {
    case INPUT_GROUP_SIZES.LG:
      return INPUT_GROUP_FIELD_PROPS.LG
    case INPUT_GROUP_SIZES.MD:
      return INPUT_GROUP_FIELD_PROPS.MD
    case INPUT_GROUP_SIZES.SM:
      return INPUT_GROUP_FIELD_PROPS.SM
    default:
      return INPUT_GROUP_FIELD_PROPS.MD
  }
}

export const getInputGroupHeight = (size: InputGroupSize): InputGroupFieldProps => {
  switch (size) {
    case INPUT_GROUP_SIZES.LG:
      return INPUT_GROUP_FIELD_PROPS.LG
    case INPUT_GROUP_SIZES.MD:
      return INPUT_GROUP_FIELD_PROPS.MD
    case INPUT_GROUP_SIZES.SM:
      return INPUT_GROUP_FIELD_PROPS.SM
    default:
      return INPUT_GROUP_FIELD_PROPS.MD
  }
}

export const getButtonColor = (variant: string): ButtonColorProps => {
  switch (variant) {
    case 'dangerField':
      return {
        background: 'frethanCrimsonRed',
        color: 'frethanWhite'
      }
    case 'disabledField':
      return {
        background: 'frethanMercury',
        color: 'frethanGrey'
      }
    case 'successField':
      return {
        background: 'frethanPositiveGreen',
        color: 'frethanWhite'
      }
    default:
      return {
        background: 'frethanUltramarineBlue',
        color: 'frethanWhite'
      }
  }
}

export const getCheckboxIconSize = (size: CheckboxSizes): string => {
  switch (size) {
    case 'lg':
      return '24px'
    case 'md':
      return '20px'
    case 'sm':
      return '16px'
    default:
      return '20px'
  }
}

export const getCheckboxIconColor = (variant: CheckboxVariants) => {
  switch (variant) {
    case 'plain':
      return colors.frethanUltramarineBlue
    case 'solid':
      return colors.frethanSoftBlue
    case 'glow':
      return colors.frethanSoftBlue
    case 'danger':
      return colors.frethanCrimsonRed
    case 'disabled':
      return colors.frethanIron
  }
}

export const getBadgeVariantProps = (variant: BadgesVariants) => {
  switch (variant) {
    case BadgeVariant.PLAIN:
      return BADGES_VARIANTS.PLAIN
    case BadgeVariant.SOLID:
      return BADGES_VARIANTS.SOLID
    case BadgeVariant.GLOW:
      return BADGES_VARIANTS.GLOW
    case BadgeVariant.DANGER:
      return BADGES_VARIANTS.DANGER
    case BadgeVariant.DISABLED:
      return BADGES_VARIANTS.DISABLED
    default:
      return BADGES_VARIANTS.PLAIN
  }
}

export const getStepperFontColors = ({
  isDisabled = false,
  isInvalid = false,
  isSolid = false,
  colorMode = 'light'
}: GetStepperFontColorsProps) => {
  if (isInvalid) return colorMode === 'light' ? 'frethanError' : 'frethanErrorText'
  if (isDisabled) return colorMode === 'light' ? 'frethanDisabled' : 'frethanDisabledDarkMode'
  if (isSolid) return colorMode === 'light' ? 'frethanBlack' : 'frethanWhite'
  return colorMode === 'light' ? 'tertiaryTypography.500' : 'frethanBlueDarkMode'
}

export const setStepIdStorage = (
  stepId: SignupStep.USER_DETAILS |
    SignupStep.VERIFY_TYPE |
    SignupStep.VERIFY |
    SignupStep.CATEGORIES |
    SignupStep.ACC_REVIEW
) => {
  localStorage.setItem('stepId', String(stepId))
}

export const getStepIdStorage = (): number => {
  const stepId = localStorage.getItem('stepId')
  return stepId && stepId !== '' ? Number(stepId) : 0
}

export const countryToFlag = (isoCode: string) => {
  return typeof String.fromCodePoint !== "undefined"
    ? isoCode
      .toUpperCase()
      .replace(/./g, (char) =>
        String.fromCodePoint(char.charCodeAt(0) + 127397)
      )
    : isoCode;
};
