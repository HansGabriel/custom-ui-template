import { ComponentWithAs, Icon, IconProps } from '@chakra-ui/react';
import React from 'react';
export const RightCarretIcon: ComponentWithAs<'svg', IconProps> = (props: IconProps) => (
  <Icon viewBox={`0 0 7 12`} fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
    <path fillRule="evenodd" clipRule="evenodd" d="M0.528596 0.195262C0.788946 -0.0650874 1.21106 -0.0650874 1.47141 0.195262L6.80474 5.5286C7.06509 5.78895 7.06509 6.21106 6.80474 6.4714L1.47141 11.8047C1.21106 12.0651 0.788946 12.0651 0.528596 11.8047C0.268247 11.5444 0.268247 11.1223 0.528596 10.8619L5.39052 6L0.528596 1.13807C0.268247 0.877722 0.268247 0.455612 0.528596 0.195262Z" 
    fill="currentColor"/>
  </Icon>
);
