import { ComponentWithAs, Icon, IconProps } from '@chakra-ui/react';
import React from 'react';

interface CheckIconBoxProps extends IconProps {
  checkBgColor?: string
  checkColor?: string
}


export const CheckIconBox: ComponentWithAs<'svg', CheckIconBoxProps> = (props: CheckIconBoxProps) => (
  <Icon viewBox={`0 0 24 24`} xmlns="http://www.w3.org/2000/svg" {...props}>
    <rect width="24" height="24" rx="6" fill={props.checkBgColor}/>
    <path fillRule="evenodd" clipRule="evenodd" d="M20.0896 6.41073C20.415 6.73617 20.415 7.26381 20.0896 7.58925L10.0896 17.5892C9.76414 17.9147 9.23651 17.9147 8.91107 17.5892L3.91107 12.5892C3.58563 12.2638 3.58563 11.7362 3.91107 11.4107C4.23651 11.0853 4.76414 11.0853 5.08958 11.4107L9.50033 15.8215L18.9111 6.41073C19.2365 6.0853 19.7641 6.0853 20.0896 6.41073Z"
      fill={props.checkColor} />
  </Icon>
)