import React, { FC, MouseEventHandler, ReactElement } from 'react'
import {
  Button,
  IconButton,
  useColorModeValue,
  Text,
  ButtonProps,
} from '@chakra-ui/react'
import { COLORSCHEME, SIZE } from '../../constants/options'

export enum buttonType {
  icon = 'icon',
  text = 'text',
}

type CustomButtonProps = {
  icon?: ReactElement | any
  leftIcon?: ReactElement | null
  content?: any
  isLoading?: boolean
  rightIcon?: ReactElement | null
  backgroundColor?: string
  colorScheme?: COLORSCHEME | string
  textColor?: string
  size?: SIZE
  variant: 'ghost' | 'outline' | 'solid' | 'link' | 'unstyled'
  onClick?: MouseEventHandler
  type?: buttonType | string
  buttonType?: 'button' | 'submit' | 'reset'
  disabled?: boolean
  buttonProps?: ButtonProps
}

export const CustomButton: FC<CustomButtonProps> = ({
  icon,
  leftIcon,
  content,
  isLoading,
  rightIcon,
  backgroundColor,
  colorScheme,
  textColor,
  size,
  variant,
  onClick,
  type,
  disabled,
  buttonType,
  buttonProps,
}) => {
  const color = useColorModeValue('white', 'gray.800')
  return type !== 'icon' ? (
    <Button
      {...buttonProps}
      type="submit"
      disabled={disabled}
      style={{ borderRadius: 8 }}
      leftIcon={leftIcon || undefined}
      rightIcon={rightIcon || undefined}
      bg={backgroundColor}
      colorScheme={colorScheme}
      textColor={textColor}
      size={size}
      onClick={onClick}
      variant={variant}
      isLoading={isLoading}
    >
      <Text>{content}</Text>
    </Button>
  ) : (
    <IconButton
      icon={icon || undefined}
      style={{ borderRadius: 8 }}
      bg={backgroundColor}
      colorScheme={colorScheme}
      textColor={textColor}
      size={size}
      variant={variant}
      isLoading={isLoading}
      aria-label={''}
    />
  )
}

CustomButton.defaultProps = {
  leftIcon: undefined,
  rightIcon: undefined,
  colorScheme: COLORSCHEME.link,
  size: SIZE.md,
  onClick: undefined,
  isLoading: false,
}
