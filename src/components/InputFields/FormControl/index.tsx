/* eslint-disable react-hooks/rules-of-hooks */
import {
    Box,
    FormControl as ChakraFormControl,
    FormErrorMessage,
    FormHelperText,
    FormLabel,
    InputGroup,
    useColorModeValue,
  } from "@chakra-ui/react";
  import React, { ReactNode } from "react";


  type InputGroupSize = "lg" | "md" | "sm";
  
  interface InputFieldProps {
    label?: string;
    isRequired?: boolean;
    description?: string;
    helperText?: string;
    type?: string;
    disableGutter?: boolean;
    disabled?: boolean;
    isInvalid?: boolean;
    size?: InputGroupSize;
    errorMessage?: string;
    placeholder?: string;
    leftIcon?: ReactNode;
    rightIcon?: ReactNode;
    value?: string | number;
    onChange?: (e: any) => void;
  }
  
  interface FormControlProps extends InputFieldProps {
    children: ReactNode;
  }
  export const FormControl = ({
    label,
    description,
    helperText,
    disableGutter,
    isInvalid,
    errorMessage,
    isRequired,
    children,
  }: FormControlProps) => {
  
    return (
      <ChakraFormControl
        mb={disableGutter ? "0" : "1.5rem"}
        isInvalid={isInvalid}
        isRequired={isRequired}
      >
        <FormLabel
          color={useColorModeValue(
            "onColorTypography.200",
            "onColorTypography.500"
          )}
          display="flex"
          mb={!!description || !label ? 0 : ".5rem"}
          justifyContent="space-between"
          fontSize={14}
        >
          {label}
        </FormLabel>
        {!!description ? (
          <Box
            color={useColorModeValue(
              "onColorTypography.200",
              "onColorTypography.500"
            )}
            mb=".5rem"
            fontSize={12}
          >
            {description}
          </Box>
        ) : (
          ""
        )}
        <InputGroup>{children}</InputGroup>
        {isInvalid ? (
          <FormErrorMessage
            fontSize={12}
            color={useColorModeValue("frethanError", "frethanErrorText")}
          >
            {errorMessage}
          </FormErrorMessage>
        ) : (
          <FormHelperText
            fontSize={12}
            color={useColorModeValue(
              "tertiaryTypography.500",
              "tertiaryTypography.200"
            )}
          >
            {helperText}
          </FormHelperText>
        )}
      </ChakraFormControl>
    );
  };
  