import {
  Box,
  Text
} from '@chakra-ui/react'
import { BadgeSelectProps, BadgesVariants } from '@/lib/types'
import { MultipleSelect } from './MultipleSelect'

interface BadgeProps {
  label?: string
  isOptional?: boolean
  description?: string
  helperText?: string
  width?: number
  variant?: BadgesVariants
  selectionList: BadgeSelectProps[] | []
}

export const SearchBadges = ({
  description,
  helperText,
  label,
  isOptional,
  variant,
  width,
  selectionList
}: BadgeProps) => {
  const defaultWidth = width ? `${width}px` : '311px'
  
  return (
    <Box>
      <Box mb="8px">
        <Text variant="labelMed" color="frethanBlack" mb="4px">
          {label} {!!isOptional && <Text as="span" color="frethanGrey">(optional)</Text>}
        </Text>
        <Text variant="captionMed" color="frethanDarkGrey">
          {description}
        </Text>
      </Box>
      <MultipleSelect 
        variant={variant}
        inputProps={{
          width: defaultWidth
        }}
      />
      {
        helperText && helperText !== '' && (
          <Text variant="captionMed" zIndex="1" color="frethanGrey" mt="4px">
            {helperText}
          </Text>
        )
      }
    </Box>
  )
}