/* eslint-disable react-hooks/rules-of-hooks */
import { FormControl } from "@/components/forms/FormControl";
import { ArrowDownIcon } from "@/components/icons/ArrowDownIcon";
import { countries } from "@/constants/options";
import { countryToFlag } from "@/lib/utils";
import { ChevronDownIcon } from "@chakra-ui/icons";
import {
  Input,
  InputLeftElement, Menu,
  MenuButton,
  MenuItem,
  MenuList,
  useColorModeValue
} from "@chakra-ui/react";
import React, { useState } from "react";
import { InputFieldProps } from "../InputField";

// eslint-disable-next-line react/display-name
export const FlagInput = React.forwardRef<HTMLInputElement, InputFieldProps>(
  (
    {
      label,
      description,
      helperText,
      type,
      disableGutter,
      disabled,
      isInvalid,
      size,
      errorMessage,
      isRequired,
      placeholder,
      leftIcon,
      rightIcon,
      ...props
    },
    ref
  ) => {
    const [country, setCountry] = useState({
      code: "PH",
      label: "Philippines",
      phone: "63",
    });
    const onSelectCountry = (
      e: React.SetStateAction<{ code: string; label: string; phone: string }>
    ) => {
      setCountry(e);
    };

    const iconColor = isInvalid
      ? useColorModeValue("frethanError", "frethanErrorText")
      : disabled
      ? useColorModeValue("frethanDisabled", "frethanDisabledDarkMode")
      : useColorModeValue("tertiaryTypography.500", "primaryTypography.200");

    return (
      <FormControl
        label={label}
        description={description}
        helperText={helperText}
        disableGutter={disableGutter}
        isInvalid={isInvalid}
        errorMessage={errorMessage}
        isRequired={isRequired}
      >
        <InputLeftElement h="100%" left="10px" color={iconColor}>
          <Menu>
            <MenuButton transition="all 0.2s">
              {countryToFlag(country.code)}
              <ArrowDownIcon marginInlineStart={1} />
            </MenuButton>
            <MenuList>
              {countries.map((option, idx) => (
                <MenuItem
                  display="flex"
                  gap={3}
                  key={idx}
                  onClick={() => onSelectCountry(option)}
                >
                  {countryToFlag(option.code)}
                  <span>
                    {option.label} ({option.code}) +{option.phone}
                  </span>
                </MenuItem>
              ))}
            </MenuList>
          </Menu>
        </InputLeftElement>

        <Input
          pl="3.5rem"
          ref={ref}
          {...props}
          value={`+${country?.phone}` || ""}
          size={size || "lg"}
          isDisabled={disabled}
          _placeholder={{
            color: isInvalid
              ? useColorModeValue("rgba(225, 62, 62, .5)", "frethanErrorText")
              : useColorModeValue("#6E7DB7", "#94A3D9"),
          }}
          placeholder={placeholder}
          type="text"
        />
      </FormControl>
    );
  }
);
