import { ComponentWithAs, Icon, IconProps } from '@chakra-ui/react';
import React from 'react';

export const ArrowLeftIcon: ComponentWithAs<'svg', IconProps> = (props: IconProps) => (
  <Icon viewBox="0 0 18 16" xmlns="http://www.w3.org/2000/svg" {...props}>
    <path d="M7.29289 15.7071C7.68342 16.0976 8.31658 16.0976 8.70711 15.7071C9.09763 15.3166 9.09763 14.6834 8.70711 14.2929L3.41421 9H17C17.5523 9 18 8.55229 18 8C18 7.44772 17.5523 7 17 7H3.41421L8.70711 1.70711C9.09763 1.31658 9.09763 0.683418 8.70711 0.292894C8.31658 -0.0976312 7.68342 -0.0976312 7.29289 0.292894L0.292894 7.29289C-0.0976312 7.68342 -0.0976312 8.31658 0.292894 8.70711L7.29289 15.7071Z"
      fill="currentColor" />
  </Icon>
)