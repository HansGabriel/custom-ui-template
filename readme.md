# Instructions
#### This README provides instructions/guide on how to implement the necessary components from the Figma design into a functional, pure component.

## Getting Started
To get started, you will need access to the Figma design file and a code editor of your choice to write your code.

### *BTW. You can play around under the example folder. 


### Design Implementation
1. Open the Figma design file and examine the design components that you want to replicate. Take note of the colors, typography, spacing, and other design elements used in the components.

2. In your code editor, create a new file for your component. This should be a pure component, meaning it should be a function that takes in props and returns the necessary JSX.

3. Write the necessary HTML for the component, using the design details that you extracted from the Figma file to ensure that your HTML code matches the design.

4. Write the necessary CSS for the component, using the design details that you extracted from the Figma file to ensure that your CSS code matches the design.

5. If your component requires any JavaScript functionality, write the necessary JavaScript code.

6. Test your component in your browser. Make sure that it looks and functions as intended.

7. Refine your component as necessary. If there are any issues or discrepancies between your component and the design in the Figma file, make the necessary adjustments to your code. Test your component again to ensure that it is functioning correctly.

Implement your component in your app. Use the code that you wrote for your component and integrate it into the app.


## Conclusion
By following these instructions, you should now have successfully replicated the necessary components from the Figma design.


reference:

- [FIGMA - design](https://www.figma.com/file/9oh6FJEVoVDwHCLWbHT0tr/Design-System-2022-(Copy)?node-id=0%3A1&t=OvTvSBTYxwddldmk-0)

- [Storybook](https://storybook.js.org/)